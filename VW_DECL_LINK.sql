ALTER view [dbo].[VW_DECL_LINKS] as

select dl.chunk_key, dl.declaration_key, dl.contract_type 
from DECL_LINKS dl
where dl.CONTRACT_TYPE = 2

union 

-- Add dummy sales declaration key, for those lots not already sold.
select pd.chunk_key, 0 as declaration_key, 2 as contract_type 
from property_documents pd 
where pd.CHUNK_KEY not in (select dl.CHUNK_KEY from DECL_LINKS dl where dl.CONTRACT_TYPE = 2)

union 

-- Duplicate all the purchase declaration keys in the view
select dl.chunk_key, dl.declaration_key, 1 as contract_type 
from decl_links dl, PROPERTY_DOCUMENTS pd 
where dl.contract_type=1 and dl.CHUNK_KEY = pd.CHUNK_KEY 

union 

-- For all completely unknown lots, add dummy purchase declaration key
select pd.chunk_key, 0 as declaration_key, 1 as contract_type 
from property_documents pd 
where pd.net_weight = pd.net_weight_unknown 

GO


